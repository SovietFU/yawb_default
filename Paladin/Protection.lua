--Abilites
local SPELL_CONSECRATION = Spell(26573, 10)
local SPELL_JUDGMENT = Spell(275779)
local SPELL_AVENGER_SHIELD = Spell(31935)
local SPELL_HAMMER_OF_THE_RIGHTEOUS = Spell(53595)
local SPELL_SHIELD_OF_THE_RIGHTEOUS = Spell(53600)
local SPELL_LIGHT_OF_THE_PROTECTOR = Spell(184092)
local SPELL_LAY_ON_HANDS = Spell(633)

--Talents
local SPELL_BLESSED_HAMMER = Spell(204019, 10)

--Buffs
local AURA_SHIELD_OF_THE_RIGHTEOUS = 132403
local AURA_CONSECRATION = 188370




local Protection = {}

function Protection.DoCombat(player, target)
	if #player:GetNearbyUnits(12) > 1 then
		Protection.AoERotation(player, target)
	else
		Protection.SingleRotation(player, target)
	end
end

function Protection.AoERotation(player, target)
	

	Protection.SingleRotation(player, target)
end

function Protection.SingleRotation(player, target)
		if not player:HasAura(AURA_CONSECRATION) and SPELL_CONSECRATION:CanCast() then
		SPELL_CONSECRATION:Cast(player)
		return
	end
	
	if player:GetHealthPercent() < 60 and SPELL_LIGHT_OF_THE_PROTECTOR:CanCast() then
		SPELL_LIGHT_OF_THE_PROTECTOR:Cast(player)
		return
	end

	if player:GetHealthPercent() < 10 and SPELL_LAY_ON_HANDS:CanCast(player) then
		SPELL_LAY_ON_HANDS:Cast(player)
		return
	end
	
	if SPELL_JUDGMENT:CanCast(target) then
		SPELL_JUDGMENT:Cast(target)
		return
	end
	
	if SPELL_AVENGER_SHIELD:CanCast(target) then
		SPELL_AVENGER_SHIELD:Cast(target)
		return
	end

	if player:HasAura(AURA_CONSECRATION) and SPELL_HAMMER_OF_THE_RIGHTEOUS:CanCast(target) then
		SPELL_HAMMER_OF_THE_RIGHTEOUS:Cast(target)
		return
	end

	if SPELL_BLESSED_HAMMER:CanCast(target) then
		SPELL_BLESSED_HAMMER:Cast(player)
		return
	end
end

return Protection